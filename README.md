
# Project Name
This code implements the feature extraction process described in the IROS 2017 paper, 
 *Feature discovery and visualization of robot mission data using convolutional autoencoders and Bayesian nonparametric topic models.*
 The code will extract discrete visual "words" from an image, cluster them using k-Means, and output a list of words for each image. This word
 list can then be used with any topic model. To reproduce the results from the paper, the [ROST topic model](https://gitlab.com/warplab/rost-cli) can
 be downloaded here. Further instructions on hyperparameter selection and running the ROST model can be found later in this tutorial.
 
## Installation
Clone the CAE directory onto your computer. In the CAE directory, create a folder and place an image stream of .jpg images in this folder. If your image data is 
some other format, the extension must be changed in the code.  The Mission I dataset used in the paper is avaliable [here](http://oort.whoi.edu/~genevieve/panama_03/d20150403_2.tar.gz).
Set the enviorment variable $DATA_HOME to the directory containing the images. This can be done manually, added to your .bashrc, or using the setup.sh bash script.
## Usage
TODO: Write usage instructions
## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
## History
TODO: Write history
## Credits
TODO: Write credits
## License
TODO: Write license



### (1) Download or aquire image dataset
The Mission I dataset from the IROS 2017 paper is avaliable here:
http://oort.whoi.edu/~genevieve/panama_03/d20150403_2.tar.gz

### (1) Set the environment variable DATA_HOME to the directory where your data folder lives
### (2) Create downsampled, square image pickle files using make_dataset.py
To use make_dataset.py
1. Place all of your images in a folder within your DATA_HOME directory. 
2. Open ```make_dataset.py``` and set USER_KEY in the user configurables section to a function object that takes a filename as input and returns an interger that allows the files to be sorted chronologically.
2. Run make_dataset.py, and use the flags to specify options. The ```--help``` flag displays parameter options. Set the following flags:
    - ```--image_size```: the desried downsampled image size, in pixels x pixels
    - ```--image_channels```: dimension of the origigonal image. 3 for RGB images, 1 for greyscale
    - ```--pickle_prefix```: the range of pixels values, 0 - PIXEL_DEPTH. Standard: 255
    - ```--file_chunk```: number of image files per pickle file. If you choose a large IMAGE_SIZE, you may have to choose a smaller FILE_CHUNK
    - ```--pickle_path```: where you want to store the resulting pickle files
    - ```--data_path```: where the image files can be found

4. Run ``python make_dataset.py`` to create a dataset
    
5. Final pickle files will be saved to the dir specified in PICKLE_PATH

### (3) Run the convolutional autoencoder
To use convolutional_autoencoder.py
1. Run convolutional_autoencoder.py, and use the flags to specify options. The ```--help``` flag displays parameter options; all have sensible default vaules execpt the following, which should be set according to the parameters used in make_dataset.py and your own file structure
    - ```--image_size```: the downsampled image size, in pixels x pixels set by make_dataset.py
    - ```--image_channels```: dimension of the original image. 3 for RGB images, 1 for greyscale, set by make_dataset.py
    - ```--pickle_prefix```: prefix of the stored pickle file, set by make_dataset.py
    - ```--intermed_path```: directory where you want the LCA images to be stored
    - ```--recon_path```: directory where you want the reconstructed images to be stored
 
    
2. If you want to change the network architecture, the line ```ae = graph.autoencoder( ... )``` can be adjusted.
3. Create the empty directory "checkpoint" in the same folder as convolutional_autoencoder.py. After 5 epochs, if the reconstruction cost is better than the last recorded cost, the model will be saved in checkpoint. If you run the code again, the model in checkpoint will be restored and immediately tested. The ```--continues``` flag can be used to continue training on a model previously saved in checkpoint.
   
5. Run ```python convolutional_autoencoder.py``` with any desired flags.
