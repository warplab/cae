"""
A convolutional autoencoder for image data, with options for configurable number of 
layers, dropout, and weight regularization w/ Tensorflow. 

Code adapted from A convolutional autoencoder tutorial,  Parag K. Mital, Jan 2016
"""
import tensorflow as tf
import numpy as np
import argparse
import glob
import math
import sys 
import os
from scipy import misc 
from scipy import ndimage
import matplotlib.pyplot as plt 
from six.moves import cPickle as pickle
import PIL.ImageOps 
from PIL import Image 
from PIL import ImageEnhance
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import sparse_ops
from tensorflow.python.ops import gen_nn_ops
import pickleUtils as Utils 
from libs.activations import lrelu
from libs.utils import corrupt 

root_dir = os.environ['DATA_HOME']; 

# Define the graph structure of the autoencoder network
class GraphStructure:
    def __init__(self, STRIDE, WEIGHT, IMAGE_SIZE):
        self.STRIDE = STRIDE
        self.WEIGHT = WEIGHT 
        self.IMAGE_SIZE = IMAGE_SIZE
        return

    def autoencoder(self, n_filters=[10, 50, 100], filter_sizes=[10, 10, 5, 5]):
        """Build a deep autoencoder w/ tied weights.
        Parameters
        ----------
        input_shape : list, optional
        n_filters : list, optional
        filter_sizes : list, optional

        Returns
        -------
        x : Tensor
            Input placeholder to the network
        z : Tensor
            Inner-most latent representation
        y : Tensor
            Output reconstruction of the input
        cost : Tensor
            Overall cost to use for training
        """
        # input to the network
        input_shape = [None, self.IMAGE_SIZE, self.IMAGE_SIZE, NUM_CHANNELS]

        x = tf.placeholder(tf.float32, input_shape, name='x') 
        current_input = x;
        x_tensor = x;

        # Implement dropout
        dropout_keep_prob = tf.placeholder(tf.float32)

        # Build the encoder
        encoder = [] # Encoder holds a list of the layer weight matrices
        shapes = []
        hidden_layers = []
        
        print "Building autoencoder network"
        for layer_i, n_output in enumerate(n_filters):
            
            n_input = current_input.get_shape().as_list()[3]
            shapes.append(current_input.get_shape().as_list())

            print "Layer:", layer_i
            print "Input layer size:", current_input.get_shape()
            print "Filter size:", filter_sizes[layer_i]

            with tf.name_scope('hidden') as scope:
                W = tf.Variable(tf.random_uniform(
                    [filter_sizes[layer_i], filter_sizes[layer_i], n_input, n_output],
                    -1.0 / math.sqrt(n_input),
                    1.0 / math.sqrt(n_input)), 
                    name='W'+str(layer_i))

                b = tf.Variable(tf.zeros([n_output]), name = "b" + str(layer_i))
                encoder.append(W)

                # lrelu preforms a leaky relu function with a configurable leakage prarmeter, default 0.2
                if layer_i == len(n_filters):
                    output = lrelu(tf.add(tf.nn.conv2d(current_input, W, strides=[1, 1, 1, 1], padding='SAME'), b))
                else:
                    output = lrelu(tf.add(tf.nn.conv2d(current_input, W, strides=[1, self.STRIDE, self.STRIDE, 1], padding='SAME'), b))

                if DO_DROPOUT:
                    output = tf.nn.dropout(output, dropout_keep_prob)

                print "Output layer size:", output.get_shape()
                hidden_layers.append(output)

                # Reset current input to be passed to the next layer
                current_input = output
                print "----------"

        # store the latent representation
        z = current_input
        print "Latent channel shape:", z.get_shape()

        # Reverse the weight matrices for decoder layer
        encoder.reverse()
        shapes.reverse()

        # Build the decoder using the same weights
        for layer_i, shape in enumerate(shapes):
            W = encoder[layer_i] # Use the same weight
            b = tf.Variable(tf.zeros([W.get_shape().as_list()[2]]))

            deconv = tf.nn.conv2d_transpose(
                            current_input,
                            filter = W,
                            output_shape = tf.pack([tf.shape(x)[0], shape[1], shape[2], shape[3]]),
                            strides=[1, self.STRIDE, self.STRIDE, 1], 
                            data_format='NHWC',
                            padding='SAME')

            output = lrelu( tf.add(deconv, b))

            current_input = output

        # now have the reconstruction through the network
        y = current_input

        # cost function measures pixel-wise difference
        if DO_WEIGHT:
            weight = self.WEIGHT
        else:
            weight = 0

        cost = tf.reduce_sum(tf.square(y - x_tensor))  \
                 + weight * tf.reduce_sum(tf.abs([tf.reduce_sum(hidden) for hidden in hidden_layers]))

        return {'x': x, 'z': z, 'y': y, 'cost': cost, 'dropout_keep_prob': dropout_keep_prob}


if __name__ == '__main__':
    # Argument parse structure
    parser = argparse.ArgumentParser(description="Run a convolutional autoencoder on image data. Outputs activations.csv (the channel activations at each timestep), \
            norm_activation.csv (the normalized activations), perplexity.csv (the image reconstruction error at each timestep) and writes the latent channel activations  \
            to the directory specified by intermed_path and the reconstructed image to the path specified by recon_path.")

    parser.add_argument("-i", "--image_size", type = int, default = 400, help = "Size of the downsampled images, in pixels by pixels.")
    parser.add_argument("-c", "--image_channels", type = int, default = 3, help = "Number of color channels in image.")
    parser.add_argument("-p", "--pickle_prefix", default = 'missionI', help = "Prefix of the pickle files containing the data.")
    parser.add_argument("-m", "--intermed_path", default = os.path.join(root_dir, 'intermed/'), help = "Path to store intermediate data representations.")
    parser.add_argument("-n", "--recon_path", default = os.path.join(root_dir, 'recon/'), help = "Path to store reconstruction images.")
    parser.add_argument("--num_pickles", type = int, default = 11, help = "The number of pickle files your data are stored in.")
    parser.add_argument("--num_images", type = int, default = 1116, help = "The total number of image files within the pickles.")
    parser.add_argument("-s", "--stride", type = int, default = 2, help = "Convolutional stride.")
    parser.add_argument("-w", "--window", type = int, default = 10, help = "Size of the convolutional filter.")
    parser.add_argument("-o", "--output", type = int, default = 5, help = "Number of output channels in the bottleneck layer")
    parser.add_argument("-e", "--epochs", type = int, default = 400, help = "Number of epochs.")
    parser.add_argument("-b", "--batch_size", type = int, default = 100, help = "Batch size for stochastic gradient descent.")
    parser.add_argument("-d", "--dropout", default = False, help = "True/False flag to use or not use dropout", action = "store_true")
    parser.add_argument("-k", "--keep_prob", type = float, default = 0.8, help = "If using dropuout, the probability a neuron is kept")
    parser.add_argument("-r", "--regularize", default = True, help = "True/False flag to use or not use layer sparsity weight regularization", action = "store_true")
    parser.add_argument("-g", "--reg_weight", type = float, default = 0.01, help = "Value of the weight regularization multiplier")
    parser.add_argument("--continues", default = False, help = "True/False flag to continue training on previously stored model. Requires a previous model stored in directory checkpoint", action = "store_true")
    args = parser.parse_args()

    IMAGE_SIZE = args.image_size;
    NUM_CHANNELS = args.image_channels
    PICKLE_RAW = args.pickle_prefix
    '''
    IMAGE_PATH = args.intermed_path
    RECON_PATH = args.recon_path
    NUM_FILES = args.num_pickles
    NUM_IMAGES = args.num_images
    '''
    STRIDE = args.stride
    WINDOW = args.window
    OUTPUT = args.output
    DO_DROPOUT= args.dropout
    DROPOUT_PROB = args.keep_prob
    DO_WEIGHT = args.regularize
    WEIGHT = args.reg_weight

    n_epochs = args.epochs
    batch_size = args.batch_size

    # Added to automate finding the number of pickles files and the number of images files
    IMAGE_PATH = args.intermed_path
    RECON_PATH = args.recon_path

    if not os.path.exists(IMAGE_PATH):
        os.makedirs(IMAGE_PATH)
    if not os.path.exists(RECON_PATH):
        os.makedirs(RECON_PATH)

    image_files = glob.iglob(os.path.join(root_dir, 'images/*.jpg'))
    pickle_files = glob.iglob(os.path.join(root_dir, 'pickles/*' + PICKLE_RAW + '*.pickle'))

    NUM_IMAGES = len(list(image_files))
    NUM_FILES = len(list(pickle_files))

    # PICKLE_FILE = args.pickle_prefix + '.400.all.'
    # End addtion

    # Initialize data array and read data from pickle files
    X = np.zeros((NUM_IMAGES, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))
    mean_img = 0;
    for pickle_file in pickle_files:
        #pickle_file = os.path.join(root_dir, 'data/full_pickles/' + PICKLE_FILE + str(i) + '.pickle')
        print "Loading file", pickle_file
        temp = Utils.load_pickled_dataset(pickle_file)
        X[i*100:i*100+temp['x'].shape[0]] = temp['x']

    data = {}
    data['train_X'] = X;

    # Find the mean image in the dataset
    mean_img = np.mean(data['train_X'])

    # Create the network architecture. The number of output layers in each encoding layer is given by n_filters, 
    # and the size of the convolutional layer used at each encoding layer is given by filter_sizes
    graph  = GraphStructure(STRIDE, WEIGHT, IMAGE_SIZE)
    ae = graph.autoencoder(n_filters=[OUTPUT/2, OUTPUT/2, OUTPUT/2, OUTPUT], filter_sizes=[WINDOW, WINDOW, 3, 3, 1])

    # The learning (decaying) learning rate and optimizer 
    learning_rate = 0.01
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(ae['cost'])

    # We create a session to use the graph
    sess = tf.Session()
    train_writer = tf.train.SummaryWriter('./train', sess.graph)
    sess.run(tf.global_variables_initializer())

    # Fit all training data
    saver = tf.train.Saver()
    best_loss = float("inf") # Change this!
    time_since_best = 0;

    # Restore the model that produced the highest validation
    # accuracy during training
    nothing = False;
    ckpt = tf.train.get_checkpoint_state('./checkpoint')
    if ckpt and ckpt.model_checkpoint_path:
        saver.restore(sess, ckpt.model_checkpoint_path)
        print "Found and restored previously saved model."
        nothing = False;
    else:
        print "No checkpoint found, training model."
        nothing = True;

    if args.continues or nothing:
        print "Training model."
        for epoch_i in range(n_epochs):
            for batch_i in xrange(data['train_X'].shape[0] // batch_size):
                offset = (batch_i * batch_size) % (data['train_X'].shape[0] - batch_size)
                batch_xs = (data['train_X'])[offset:(offset + batch_size), :, :, :]

                train = np.array([img - mean_img for img in batch_xs])

                sess.run(optimizer, feed_dict={ae['x']: train, ae['dropout_keep_prob']: DROPOUT_PROB})

            cost = sess.run(ae['cost'], feed_dict={ae['x']: train, ae['dropout_keep_prob']: 1.0})

            print "Epoch:", epoch_i, "\t |  Cost:", cost # Print the current epoch and current cost

            # If at least 5 epochs have passed and we have a better model, save current model
            time_since_best += 1;
            if time_since_best > 5 and cost < best_loss:
                print "Saving model!"
                saver.save(sess, './checkpoint/my-model')
                best_loss = cost;
                print_cost = np.zeros((1,1)).astype(int)
                print_params = np.zeros((3,1)).astype(int)
                print_cost[0][0] = best_loss
                print_params[0][0] = STRIDE 
                print_params[1][0] = WINDOW 
                print_params[2][0] = OUTPUT 
                # Save the best cost and network params as .txt files
                np.savetxt('./checkpoint/best_cost.txt', print_cost)
                np.savetxt('./checkpoint/params.txt', print_params)
                time_since_best = 0;

    num_examples = NUM_IMAGES
    n_examples = OUTPUT 

    # Calculated the activation of each channel at everyt timestep by maximizing
    # over all channels at every pixel. Outputs activations to activations.csv
    # Normalizes these activations and saves to norm_activations.csv
    # Also saves image reconstruction error at each timestep to perplexity.csv
    def generate_activations():
        # Set up zeros vectors
        activation = np.zeros((num_examples, OUTPUT+1))
        norm_activation = np.zeros((num_examples, OUTPUT+1))
        perplexity = np.zeros((num_examples, 2))

        # Iterate through each image
        for example_i in xrange(num_examples): 
            # Feed image into CAE to recover intermediate representation and reconstructed image
            x_norm = [data['train_X'][example_i, ...] - mean_img]
            intermed, reconn_error = sess.run([ae['z'], ae['cost']], feed_dict={ae['x']: x_norm, ae['dropout_keep_prob']: 1.0})
            intermed = intermed + mean_img;

            # Normalize image to range 0-255
            min_value = np.min(intermed)
            intermed = intermed - min_value
            max_value = np.max(intermed)
            intermed = intermed / max_value
            intermed = intermed * 255;

            channels = np.zeros((intermed.shape[3]+1,)).astype(int)

            '''
            for x in xrange(intermed.shape[1]):
                for y in xrange(intermed.shape[2]):
                    max_channel = np.argmax(intermed[0, x, y, :])
                    # Index plus one to account for adding the timestep as the first entry
                    #channels[max_channel+1] += 1;
            '''

            for x in xrange(intermed.shape[3]):
                sum = np.sum(intermed[0, :, :, x])
                channels[x+1] = sum;
            channels[0] = example_i;

            activation[example_i, :] = channels;
            perplexity[example_i, 0] = example_i;
            perplexity[example_i, 1] = reconn_error;

        norm_activation = (activation - np.min(activation, axis = 0) )
        norm_activation /= np.max(norm_activation, axis = 0) + 1e-7 # this is to prevent from divide by zero error
        norm_activation[0, :] = activation[0, :]

        np.savetxt(os.path.join(root_dir, 'activations.csv'), activation, delimiter=",")  
        np.savetxt(os.path.join(root_dir, 'norm_activations.csv'), norm_activation, delimiter=",")  
        np.savetxt(os.path.join(root_dir, 'perplexity.csv'), perplexity, delimiter=",", fmt='%d, %.2f')  

    # Writes the intermediate representation to image files, stored by channel name, at intermed_path
    def write_intermediate_all():
        for index in xrange(data['train_X'].shape[0]):
            test_xs = data['train_X'][[index], ...]
            test_xs_norm = np.array([img - mean_img for img in test_xs])

            recon, intermed = sess.run([ae['y'], ae['z']], feed_dict={ae['x']: test_xs_norm, ae['dropout_keep_prob']: 1.0})
            rec_int = np.uint8((recon[0, ...] + mean_img)*255 + 255./2.)
            im_recon = Image.fromarray(rec_int);
            
            image_path = os.path.join(RECON_PATH, 'index_' + str(index) + '_' + str(0) + '.jpg') 
            # print "Saving to", image_path 
            im_recon.save(image_path)

            intermed = intermed + mean_img

            min_value = np.min(intermed)
            intermed = intermed - min_value
            max_value = np.max(intermed)
            intermed = intermed / max_value

            for i in xrange(intermed[0].shape[2]):
                med_int = np.uint8((intermed[0, :, :, i]) * 255)
                im = Image.fromarray(med_int);
                all_path = os.path.join(IMAGE_PATH, 'all')
                if not os.path.exists(all_path):
                    os.makedirs(all_path)
                all_path = os.path.join(all_path, 'index_' + str(index) + '_' + str(i) + '.jpg') 
                image_path = os.path.join(IMAGE_PATH, 'ch' + str(i))
                if not os.path.exists(image_path):
                    os.makedirs(image_path)
                image_path = os.path.join(image_path, 'index_' + str(index) + '_' + str(i) + '.jpg') 

                # print "Saving to", image_path
                im.save(image_path)
                # print "Saving to", all_path 
                im.save(all_path)
    
    # Writes the intermediate representation to image files, stored by channel name, at IMAGE_PATH 
    # and image reconstructed image to RECON_PATH
    def write_intermediate_long():
        print "Saving intermediate images to", IMAGE_PATH, "and reconstructed image to", RECON_PATH

        # Iterate through each image in the dataset
        for index in xrange(data['train_X'].shape[0]):
            test_xs = data['train_X'][[index], ...]

            # Subtract mean from the image for convolution
            test_xs_norm = np.array([img - mean_img for img in test_xs])

            # Feed image to get reconstruction and intermediate LCA
            recon, intermed = sess.run([ae['y'], ae['z']], feed_dict={ae['x']: test_xs_norm, ae['dropout_keep_prob']: 1.0})

            # Reformat the reconstruction
            rec_int = np.uint8((recon[0, ...] + mean_img)*255 + 255./2.)
            im_recon = Image.fromarray(rec_int);
           
            # Save reconstruction image
            image_path = os.path.join(RECON_PATH, 'index_' + str(index) + '_' + str(0) + '.jpg') 
            im_recon.save(image_path)

            # Reformat the intermediate images
            intermed = intermed + mean_img

            # Normalize activations between 0 and 1
            min_value = np.min(intermed)
            intermed = intermed - min_value
            max_value = np.max(intermed)
            intermed = intermed / max_value

            # For each pixel in LCA
            #for i in xrange(intermed[0].shape[2]):
            print "Saving image index [", index, "]"
            for i in xrange(intermed[0].shape[0]):
                for j in xrange(intermed[0].shape[1]):
                    # Rescale activations from 0-255 and reformat
                    med_int = np.uint8((intermed[0, i, j, :]) * 255)
                    im = Image.fromarray(med_int.reshape((med_int.shape[0], 1)))

                    all_path = os.path.join(IMAGE_PATH, 'all')
                    if not os.path.exists(all_path):
                        os.makedirs(all_path)
                    all_path = os.path.join(all_path, 'index_' + str(index) + '_' + str(i*intermed[0].shape[0] + j) + '.jpg') 

                    image_path = os.path.join(IMAGE_PATH, 'ch' + str(i*intermed[0].shape[0] + j))
                    if not os.path.exists(image_path):
                        os.makedirs(image_path)
                    image_path = os.path.join(image_path, 'index_' + str(index) + '_' + str(i*intermed[0].shape[0] + j) + '.jpg') 

                    print "Saving to", image_path
                    im.save(image_path)
                    print "Saving to", all_path 
                    im.save(all_path)

    # Run the two previously defined functions
    write_intermediate_long()
    generate_activations()




