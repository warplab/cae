from six.moves import cPickle as pickle
import os

root_dir = os.environ['DATA_HOME']; 
PICKLE_PATH = os.path.join(root_dir, 'data/pickles/') 

''' Loads the pickeled training, validation, and test data from file '''
def load_pickled_dataset(pickle_file):
    with open(pickle_file, 'rb') as f:
        save = pickle.load(f)
        X = save['test_data']

    del save  # hint to help free up memory
    return {'x': X}

def save_pickle_file(pickle_file, save_dict):
    try: 
        f = open(PICKLE_PATH + pickle_file, 'wb') 
        pickle.dump(save_dict, f, pickle.HIGHEST_PROTOCOL) 
        f.close()
    except Exception as e:
        print('Unable to save data to', pickle_file, ':', e)
        raise

    print "Datasets saved to file", PICKLE_PATH + pickle_file
