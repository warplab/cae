#!/usr/bin/env python
import os, sys, glob, csv
import matplotlib as mp
import numpy as np
from bokeh.plotting import figure, output_file, output_notebook, show, vplot,gridplot
from bokeh.palettes import brewer
from bokeh.charts import Area, TimeSeries, Line
from bokeh.models import ColumnDataSource, Slider, Callback, CustomJS, TapTool, OpenURL
from bokeh.palettes import brewer
from bokeh.io import vform

NUM_CHANNELS = 20 

def get_key_virgin_islands(filename):
    return int(filename[10:-4])
def get_key_panama(filename):
    return int(filename[-10:-4])    
def get_key_intermed(filename):
    pos = [p for p, char in enumerate(filename) if char == '_']
    assert(len(pos) == 2)
    return float(filename[pos[0]+1:pos[1]]) + float(filename[pos[1]+1:-4])/10.0

def load_image_filenames(image_glob='images/*.jp*g', image_dir='.', key = get_key_panama): 
    image_dir=os.path.abspath(image_dir)
    image_filenames=[]
    image_filenames=glob.glob(image_dir+'/'+image_glob)
    while len(image_filenames)==0:
        print('Looking in '+image_dir)
        if image_dir == '' or image_dir =='/':
            print("Could not find "+image_glob)
            return []
        image_dir=os.path.dirname(image_dir)
        image_filenames=glob.glob(image_dir+'/'+image_glob)
    print('Found '+str(len(image_filenames))+' images in '+image_dir)
    image_filenames=list(map(os.path.relpath, image_filenames))
    image_filenames.sort(key = key)
    return image_filenames

def load_topic_data(topic_hist_filename='./topics.hist.csv'):
    topics=[]
    timestamps=[]
    topic_order=[]
    vocabsize=0

    data=[]
    numcols=0
    with open(topic_hist_filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            numcols = max(numcols, len(row))
            data.append(list(map(float,row)))
    for row in data:
        while len(row) < numcols:
            row.append(0)

    vocabsize=numcols-1

    data=np.array(data)
    timestamps=data[:,0]
    topics=data[:,1:]
    topic_weights=np.ones(vocabsize)
    for t in topics:
        topic_weights+=t
        t/=np.sum(t)

    W=np.sum(topic_weights)
    topic_weights/=W

    topic_H=np.zeros(len(topics))
    minus_log_weights=-np.log(topic_weights)
    for i in range(len(topics)):
        topic_H[i]=np.sum(topics[i] * minus_log_weights)

    topic_order=np.array(range(vocabsize))
    return (topics, topic_H, topic_weights, vocabsize)



def load_ppx_data(ppx_filename='./perplexity.csv'):
    data=[]
    with open(ppx_filename, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(list(map(float,row)))
    ppxdata=np.array(data)
    ppxdata=ppxdata[:,1]
    return ppxdata

output_file('index.html')
image_filenames= load_image_filenames('images/*.jpg')
recon_filenames= load_image_filenames('recon/*.jpg', key = get_key_intermed)


channel_filenames = []
for k in xrange(NUM_CHANNELS):
    names = load_image_filenames('intermed/ch' + str(k) + '/*.jpg', key=get_key_intermed)
    channel_filenames.append(names)

(topic_dist, topic_H, topic_weight, K) = load_topic_data('./activations.csv')
(topic_distN, topic_HN, topic_weightN, K) = load_topic_data('./norm_activations.csv')
topic_H = np.exp(topic_H)

word_ppx = load_ppx_data()
topic_order=np.argsort(topic_weight)[::-1]
topic_orderN=np.argsort(topic_weightN)[::-1]

topic_dist_ordered=(topic_dist.transpose())[topic_order]
topic_dist_orderedN=(topic_distN.transpose())[topic_orderN]

cummulative_topic_dist = np.cumsum(topic_dist_ordered, axis=0)
cummulative_topic_distN = np.cumsum(topic_dist_orderedN, axis=0)
timesteps=list(range(len(image_filenames)))
T=len(timesteps)

print('#timesteps: '+str(T))
print('#ppx: '+str(len(word_ppx)))
print('#h: '+str(len(topic_H)))

print('urls '+str(len(image_filenames)))

#plot_ppx_vals =  (word_ppx/np.percentile(word_ppx,99))
plot_ppx_vals =  np.log(word_ppx)
plot_h_vals = (topic_H/ np.percentile(topic_H,99))


source = ColumnDataSource(dict(
    t=[0], 
    cursor_x=[0,0], 
    cursor_y_ppx=[min(word_ppx), max(word_ppx)],
    #cursor_y_H=[min(topic_H), max(topic_H)], 
    cursor_y=[-0.05, 1.05],
    #cursor_y_H=[min(topic_H), max(topic_H)], 
    x=timesteps, 
    ppx=plot_ppx_vals, 
    h=plot_h_vals, 
    url=[image_filenames[0]], 
    recon_url=[recon_filenames[0]], 
    image_urls = image_filenames,
    image_recon_urls = recon_filenames,
    ch0_url = [channel_filenames[0][0]], 
    ch1_url = [channel_filenames[1][0]], 
    ch2_url = [channel_filenames[2][0]], 
    ch3_url = [channel_filenames[3][0]], 
    ch4_url = [channel_filenames[4][0]], 
    ch5_url = [channel_filenames[5][0]], 
    ch6_url = [channel_filenames[6][0]], 
    ch7_url = [channel_filenames[7][0]], 
    ch8_url = [channel_filenames[8][0]], 
    ch9_url = [channel_filenames[9][0]], 
    ch10_url = [channel_filenames[10][0]], 
    ch11_url = [channel_filenames[11][0]], 
    ch12_url = [channel_filenames[12][0]], 
    ch13_url = [channel_filenames[13][0]], 
    ch14_url = [channel_filenames[14][0]], 
    ch15_url = [channel_filenames[15][0]], 
    ch16_url = [channel_filenames[16][0]], 
    ch17_url = [channel_filenames[17][0]], 
    ch18_url = [channel_filenames[18][0]], 
    ch19_url = [channel_filenames[19][0]], 
    image_ch0_url = channel_filenames[0], 
    image_ch1_url = channel_filenames[1], 
    image_ch2_url = channel_filenames[2], 
    image_ch3_url = channel_filenames[3], 
    image_ch4_url = channel_filenames[4], 
    image_ch5_url = channel_filenames[5], 
    image_ch6_url = channel_filenames[6],
    image_ch7_url = channel_filenames[7], 
    image_ch8_url = channel_filenames[8], 
    image_ch9_url = channel_filenames[9],
    image_ch10_url = channel_filenames[10], 
    image_ch11_url = channel_filenames[11], 
    image_ch12_url = channel_filenames[12], 
    image_ch13_url = channel_filenames[13], 
    image_ch14_url = channel_filenames[14], 
    image_ch15_url = channel_filenames[15], 
    image_ch16_url = channel_filenames[16], 
    image_ch17_url = channel_filenames[17], 
    image_ch18_url = channel_filenames[18], 
    image_ch19_url = channel_filenames[19], 
))
"""
"""

#callback = Callback(args=dict(source=source), code="""
callback = CustomJS(args=dict(source=source), code="""
    var data = source.get('data');
    var t = time.get('value')
    data['t'][0] = t
    data['url'][0]= data['image_urls'][t]
    data['recon_url'][0]= data['image_recon_urls'][t]
    data['cursor_x']=[t,t]
    source.trigger('change');
    data['ch0_url'][0]= data['image_ch0_url'][t]
    data['ch1_url'][0]= data['image_ch1_url'][t]
    data['ch2_url'][0]= data['image_ch2_url'][t]
    data['ch3_url'][0]= data['image_ch3_url'][t]
    data['ch4_url'][0]= data['image_ch4_url'][t]
    data['ch5_url'][0]= data['image_ch5_url'][t]
    data['ch6_url'][0]= data['image_ch6_url'][t]
    data['ch7_url'][0]= data['image_ch7_url'][t]
    data['ch8_url'][0]= data['image_ch8_url'][t]
    data['ch9_url'][0]= data['image_ch9_url'][t]
    data['ch10_url'][0]= data['image_ch10_url'][t]
    data['ch11_url'][0]= data['image_ch11_url'][t]
    data['ch12_url'][0]= data['image_ch12_url'][t]
    data['ch13_url'][0]= data['image_ch13_url'][t]
    data['ch14_url'][0]= data['image_ch14_url'][t]
    data['ch15_url'][0]= data['image_ch15_url'][t]
    data['ch16_url'][0]= data['image_ch16_url'][t]
    data['ch17_url'][0]= data['image_ch17_url'][t]
    data['ch18_url'][0]= data['image_ch18_url'][t]
    data['ch19_url'][0]= data['image_ch19_url'][t]
""")

"""
"""
image_w=1360; image_h=1024
plot_w=1200; plot_h=200;

ch_w = 10; ch_h = 10
ch_plot_w= 250; 

fig_img=figure(x_range=(0,image_w), y_range=(0,image_h), width=int(plot_w/2), height=int(plot_w*image_h/image_w/2+0.5))
fig_img.image_url(url='url', source=source, x=0, y=0, angle=0, w=image_w, h=image_h, anchor='bottom_left')

fig_recon=figure(x_range=(0,image_w), y_range=(0,image_h), width=int(plot_w/2), height=int(plot_w*image_h/image_w/2+0.5))
fig_recon.image_url(url='recon_url', source=source, x=0, y=0, angle=0, w=image_w, h=image_h, anchor='bottom_left')

fig_ch = []
for i in xrange(NUM_CHANNELS):
    fig_chN = figure(x_range=(0,ch_w), y_range=(0,ch_h), width=int(ch_plot_w/2), height=int(ch_plot_w*ch_h/ch_w/2))
    fig_chN.image_url(url='ch' + str(i) + '_url', source=source, x=0, y=0, angle=0, w=ch_w, h=ch_h, anchor='bottom_left')
    fig_ch.append(fig_chN)


colormap=brewer["Spectral"][min(11,K)]
fig_topicdist=figure(width=plot_w, height=plot_h, y_range=[-0.1,1.1], title="Raw CAE Activations")
fig_topicdist.title.text_font_size = '20pt'
fig_topicdist.title.align = 'center'
for i in range(K-1,-1,-1):
    fig_topicdist.patch(x=np.hstack(([timesteps[0]],timesteps, [timesteps[-1]])), 
            y=np.hstack(([0], cummulative_topic_dist[i],[0])), 
            color=colormap[i%min(K,11)])
fig_topicdist.line(x='cursor_x', y='cursor_y', source=source, color='black')
fig_topicdist.circle(x='cursor_x', y='cursor_y', source=source, color='black', fill_color='white')
fig_topicdist.yaxis.axis_label = '|LCA|'
fig_topicdist.yaxis.axis_label_text_font_size = '15pt'
fig_topicdist.xaxis.axis_label = 'Time'
fig_topicdist.xaxis.axis_label_text_font_size = '15pt'

fig_topicdistN=figure(width=plot_w, height=plot_h, y_range=[-0.1,1.1], title="Normalized CAE Activations")
fig_topicdistN.title.text_font_size = '20pt'
fig_topicdistN.title.align = 'center'
for i in range(K-1,-1,-1):
#for i in range(9,-1,-1):
    fig_topicdistN.patch(x=np.hstack(([timesteps[0]],timesteps, [timesteps[-1]])), 
            y=np.hstack(([0], cummulative_topic_distN[i],[0])), 
            color=colormap[i%min(K,11)])
fig_topicdistN.line(x='cursor_x', y='cursor_y', source=source, color='black')
fig_topicdistN.circle(x='cursor_x', y='cursor_y', source=source, color='black', fill_color='white')
fig_topicdistN.yaxis.axis_label = 'Normalized |LCA|'
fig_topicdistN.yaxis.axis_label_text_font_size = '15pt'

"""
fig_act = figure(width=plot_w, height=plot_h, x_range=fig_topicdist.x_range, y_range=(0, 1.5))
for i in xrange(NUM_CHANNELS):
    #plot_topic_percen = cummulative_topic_dist[i]/np.percentile(cummulative_topic_dist[i], 99)
    #plot_topic_percen = -np.log(cummulative_topic_dist[i])
    plot_topic_percen = (cummulative_topic_dist[i])/np.mean(cummulative_topic_dist[i])
    fig_act.line(x = timesteps, y = plot_topic_percen, legend='Channel ' + str(i), color = colormap[i%10])
"""

fig_H=figure(width=plot_w, height=plot_h, x_range=fig_topicdist.x_range, y_range=[np.min(plot_ppx_vals),np.max(plot_ppx_vals)+0.1], title = 'Raw CAE Activations')
fig_H.title.text_font_size = '20pt'
fig_H.title.align = 'center'
#fig_H.line(x=timesteps, y=plot_h_vals, legend='Topic Perplexity')
fig_H.line(x=timesteps, y=plot_ppx_vals, color=colormap[-1], legend='Word Perplexity') #red
fig_H.legend.orientation='vertical'
fig_H.line(x='cursor_x', y='cursor_y', source=source, color='black')
fig_H.circle(x='cursor_x', y='cursor_y', source=source, color='black', fill_color='white')
fig_H.xaxis.axis_label = 'Time'
fig_H.xaxis.axis_label_text_font_size = '15pt'
fig_H.yaxis.axis_label = 'Recon. Error'
fig_H.yaxis.axis_label_text_font_size = '15pt'

time_slider = Slider(start=0, end=len(timesteps)-1, value=0, step=1, title="Time", callback=callback)

callback.args['time']=time_slider

#plots = gridplot([[fig_topicdist], [fig_topicdistN], [fig_H], [fig_act]], border_space=1)
#plots = gridplot([[fig_topicdist], [fig_topicdistN], [fig_H]], border_space=1)
#plots = gridplot([[fig_topicdist], [fig_H]], border_space=1)
plots = gridplot([[fig_topicdist]], border_space=1)
#plots = gridplot([[fig_H]], border_space=1)
#im_grid = gridplot([[fig_ch[0], fig_ch[1], fig_ch[2], fig_ch[3]], [fig_ch[4], fig_ch[5], fig_ch[6], fig_ch[7]]])
im_grid = gridplot([[fig_ch[0], fig_ch[1], fig_ch[2], fig_ch[3], fig_ch[4], fig_ch[5], fig_ch[6], fig_ch[7], fig_ch[8], fig_ch[9]], 
                    [fig_ch[10], fig_ch[11], fig_ch[12], fig_ch[13], fig_ch[14], fig_ch[15], fig_ch[16], fig_ch[17], fig_ch[18], fig_ch[19]]
                    ])
                    

#im_grid = gridplot([[fig_ch[0], fig_ch[1], fig_ch[2], fig_ch[3]]])
#im_grid = gridplot([[fig_ch[0]]])
fig_grid = gridplot([[fig_img, fig_recon]])

p = vform(fig_grid, time_slider, im_grid, plots)
#p = vform(fig_grid, time_slider, im_grid)


print ('Writing index.html')
show(p)
